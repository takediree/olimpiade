<!DOCTYPE html>
<html lang="en">

<head>
 @include('template.head')
</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

           @include('template.sidebar')

        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
               @include('template.navbar')
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                <h1 class="h3 mb-2 text-gray-800">Edit Data Peserta</h1>
        
                <form method="post" action="{{ url("alternatif/".$alternatif->id) }}" >
                    @csrf
                    @method('PUT')
                    <div class="card-body col-md-6 ">

                            <select class="form-control" aria-label="Default select example" name="id_peserta">
                                    <option selected >{{$alternatif->id_peserta}}</option>

                                    @foreach($peserta as $p)

                                    <option value="{{ $p->id }}">{{ $p->nama_siswa }}</option>

                                    @endforeach

                            </select> <br>


                                <select class="form-control" aria-label="Default select example" name="id_kriteria">
                                    <option selected>{{$alternatif->id_peserta}}</option>

                                    @foreach($kriteria as $k)

                                    <option value="{{ $k->id }}">{{ $k->kriteria }}</option>

                                    @endforeach

                            </select>



                            <div class="form-group">
                                <label for="nilai">  </label>
                                <input type="text" class="form-control" id="nilai" placeholder="Masukkan penilaian" name="nilai" value='{{$alternatif->nilai}}' required>
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <input type="button" class="btn btn-primary" value="Kembali" onclick="history.back(-1)" />
                            </div>
                    <!-- /.card-body -->



                </form>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; Your Website 2020</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="login.html">Logout</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="{{asset('sb/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('sb/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{asset('sb/vendor/jquery-easing/jquery.easing.min.js')}}"></script>

    <!-- Custom scripts for all pages-->
    <script src="{{asset('sb/js/sb-admin-2.min.js')}}"></script>

</body>

</html>