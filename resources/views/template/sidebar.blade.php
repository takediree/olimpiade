 <!-- Sidebar - Brand -->
 <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
                <div class="sidebar-brand-icon rotate-n-15">
                    <i class="fas fa-laugh-wink"></i>
                </div>
                <div class="sidebar-brand-text mx-3"><sup></sup>Admin</div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item">
                <a class="nav-link" href="{{url('home')}}">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider">

            <li class="nav-item">
                <a class="nav-link" href="{{url('kriteria')}}">
                    <i class="fa fa-th-list"></i>
                    <span> Kriteria </span></a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="{{url('peserta')}}">
                    <i class="fa fa-users"></i>
                    <span> Calon Peserta </span></a>
            </li>

            <!-- Heading -->
            {{-- <div class="sidebar-heading">
                Interface
            </div> --}}

            <!-- Nav Item - Pages Collapse Menu -->


           
            <li class="nav-item">
                <a class="nav-link" href="{{url('alternatif')}}">
                    <i class="fas fa-fw fa-chart-area"></i>
                    <span> Alternatif </span></a>
            </li>
<!-- Divider -->
<hr class="sidebar-divider">

             <li class="nav-item">
                <a class="nav-link" href="{{url('vikor')}}">
                <i class="fa fa-calculator"></i>
                <span> Hitung </span></a>
            </li>


            

            <!-- Heading -->
            {{-- <div class="sidebar-heading">
                Master Data
            </div> --}}

            

            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>
