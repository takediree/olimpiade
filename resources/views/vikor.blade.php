<!DOCTYPE html>
<html lang="en">

<head>
    @include('template.head')
</head>

<body id="page-top">

<!-- Page Wrapper -->
<div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

        @include('template.sidebar')

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">

            <!-- Topbar -->
        @include('template.navbar')
        <!-- End of Topbar -->

            <!-- Begin Page Content -->
            <div class="container-fluid">

                <h1 class="h3 mb-2 text-gray-800">Nilai Alternatif Awal</h1>

                <div class="card shadow mb-4">

                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>No</th>

                                    <th>Nama Siswa</th>

                                @if(!empty($daftar_nilai))
                                    @foreach($daftar_nilai as $data => $d)


                                            <th>{{ $kriteria[$data]->kriteria }}</th>

                                        @endforeach
                                @endif



                                </tr>
                                </thead>

                                <tbody>




                                @if(!empty($data_alternatif))
                                    @foreach($data_alternatif as $data)

                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $data["nama"] }}</td>

                                            @foreach($data["nilai"] as $nilai => $value)
                                                <td>{{ $value }}</td>

                                            @endforeach
                                        </tr>
                                    @endforeach
                                @endif

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->



            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingOne">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Tampilkan Detail Proses Algoritma VIšekriterijumsko KOmpromisno Rangiranje (VIKOR)
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">

                        <!-- Begin Page Content -->
                        <div class="container-fluid">

                            <h1 class="h3 mb-2 text-gray-800">Nilai Setelah Di Normalisasi</h1>

                            <div class="card shadow mb-4">

                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                            <thead>
                                            <tr>
                                                <th>No</th>

                                                <th>Nama Siswa</th>

                                                @if(!empty($daftar_nilai))
                                                    @foreach($daftar_nilai as $data => $d)


                                                        <th>{{ $kriteria[$data]->kriteria }}</th>

                                                    @endforeach
                                                @endif



                                            </tr>
                                            </thead>

                                            <tbody>




                                            @if(!empty($dataKriteriaNormalisasiUntukTable))
                                                @foreach($dataKriteriaNormalisasiUntukTable as $data)

                                                    <tr>
                                                        <td>{{ $loop->iteration }}</td>

                                                        @foreach($data as $value)
                                                            <td>{{ $value }}</td>

                                                        @endforeach
                                                    </tr>
                                                @endforeach
                                            @endif

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.container-fluid -->



                        <!-- Begin Page Content -->
                        <div class="container-fluid">

                            <h1 class="h3 mb-2 text-gray-800">Nilai Setelah Di Normalisasi Dikalikan Bobot</h1>

                            <div class="card shadow mb-4">

                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                            <thead>
                                            <tr>
                                                <th>No</th>

                                                <th>Nama Siswa</th>

                                                @if(!empty($daftar_nilai))
                                                    @foreach($daftar_nilai as $data => $d)


                                                        <th>{{ $kriteria[$data]->kriteria }}</th>

                                                    @endforeach
                                                @endif



                                            </tr>
                                            </thead>

                                            <tbody>




                                            @if(!empty($dataKriteriaNormalisasiUntukTable2))
                                                @foreach($dataKriteriaNormalisasiUntukTable2 as $data)

                                                    <tr>
                                                        <td>{{ $loop->iteration }}</td>

                                                        @foreach($data as $value)
                                                            <td>{{ $value }}</td>

                                                        @endforeach
                                                    </tr>
                                                @endforeach
                                            @endif

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.container-fluid -->

                        <!-- Begin Page Content -->
                        <div class="container-fluid">

                            <h1 class="h3 mb-2 text-gray-800">Nilai Index Untuk Ranking</h1>

                            <div class="card shadow mb-4">

                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                            <thead>
                                            <tr>
                                                <th>Hasil Perangkingan</th>

                                                <th>Nama Siswa</th>
                                                <th>Nilai Q</th>





                                            </tr>
                                            </thead>

                                            <tbody>




                                            @if(!empty($daftarQ))
                                                @foreach($daftarQ as $data)

                                                    <tr>
                                                        <td>{{ $loop->iteration }}</td>
                                                        <td>{{ $data["nama"] }}</td>
                                                        <td>{{ $data["nilai"] }}</td>


                                                    </tr>
                                                @endforeach
                                            @endif

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.container-fluid -->




                    </div>
                </div>

            </div>







            <!-- Begin Page Content -->
            <div class="container-fluid">

                <p>Peserta Yang Di Rekomendasi Untuk Ikut Adalah</p>
                <h1 class="h3 mb-2 text-gray-800">{{ $daftarQ[0]["nama"] }}</h1>


            </div>
            <!-- /.container-fluid -->



        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <footer class="sticky-footer bg-white">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright &copy; Your Website 2020</span>
                </div>
            </div>
        </footer>
        <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" href="login.html">Logout</a>
            </div>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript-->
<script src="{{asset('sb/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('sb/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

<!-- Core plugin JavaScript-->
<script src="{{asset('sb/vendor/jquery-easing/jquery.easing.min.js')}}"></script>

<!-- Custom scripts for all pages-->
<script src="{{asset('sb/js/sb-admin-2.min.js')}}"></script>

</body>

</html>
