<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\KriteriaController;
use App\Http\Controllers\PesertaController;
use App\Http\Controllers\AlternatifController;
use App\Http\Controllers\VikorController;





Route::get('/', [LoginController::class, 'login'])->name('login');
Route::get('/logout', [LoginController::class, 'logout']);
Route::get('/register', [RegisterController::class, 'register']);
Route::post('/register', [RegisterController::class, 'create_register']);

Route::get('/home', [HomeController::class, 'home']);


Route::post('/authenticate', [LoginController::class , 'authenticate']);

Route::group(['middleware' => 'auth'], function () {







Route::get('/home', [HomeController::class, 'home']);
Route::get('/vikor', [VikorController::class, 'index']);


Route::get('/kriteria', [KriteriaController::class, 'index']);
Route::get('/kriteria/create', [KriteriaController::class, 'create']);
Route::post('/kriteria', [KriteriaController::class, 'store']);
Route::get('/kriteria/update/{kriteria}', [KriteriaController::class, 'edit']);
Route::put('/kriteria/{kriteria}', [KriteriaController::class, 'update']);
Route::get('/kriteria/delete/{id_kriteria}', [KriteriaController::class, 'destroy']);


Route::get('/peserta', [PesertaController::class, 'index']);
Route::get('/peserta/create', [PesertaController::class, 'create']);
Route::post('/peserta', [PesertaController::class, 'store']);
Route::get('/peserta/update/{peserta}', [PesertaController::class, 'edit']);
Route::put('/peserta/{peserta}', [PesertaController::class, 'update']);
Route::get('/peserta/delete/{id}', [PesertaController::class, 'destroy']);


Route::get('/alternatif', [AlternatifController::class, 'index']);
Route::get('/alternatif/create', [AlternatifController::class, 'create']);
Route::post('/alternatif', [AlternatifController::class, 'store']);
Route::get('/alternatif/update/{alternatif}', [AlternatifController::class, 'edit']);
Route::put('/alternatif/{alternatif}', [AlternatifController::class, 'update']);
Route::get('/alternatif/delete/{id}', [AlternatifController::class, 'destroy']);
Route::get('/alternatif/tambahnilai/{id}', [AlternatifController::class, 'tambahnilai']);
Route::post('/alternatif/{alternatif}', [AlternatifController::class, 'savenilai']);

});
