<?php

namespace App\Http\Controllers;

use App\Models\Alternatif;
use App\Models\Kriteria;
use App\Models\Peserta;
use Illuminate\Http\Request;
use App\Models\User;

class VikorController extends Controller
{

   public function index()
     {

        $peserta = Peserta::all();
        $kriteria = Kriteria::all();


        //Ambil Data Alternatif
        $alternatif = Alternatif::join('tbl_kriteria' , 'tbl_kriteria.id' , '=' , 'tbl_alternatif.id_kriteria')
            ->join('tbl_peserta' , 'tbl_peserta.id' , '=' , 'tbl_alternatif.id_peserta')
            ->get();



        $data_nilai = array();
        $data_alternatif = array();

        $dataKriteria = array();



        //Ambil Nilai Per Peserta
        foreach ($peserta as $s) {


            foreach ($kriteria as $k) {




                $alternatif = Alternatif::join('tbl_kriteria', 'tbl_kriteria.id', '=', 'tbl_alternatif.id_kriteria')
                    ->join('tbl_peserta', 'tbl_peserta.id', '=', 'tbl_alternatif.id_peserta')
                    ->where('tbl_alternatif.id_kriteria', '=', $k->id)
                    ->where('tbl_alternatif.id_peserta', '=', $s->id)

                    ->first();


                if(!empty($alternatif))
                {
                    $namaKriteria = $alternatif["kriteria"];
                    $nilaiKriteria =  $alternatif["nilai"];

                    $data_nilai[] = $nilaiKriteria;
                }




            }

            $data_alternatif[] = [

                "nama" => $s->nama_siswa,
                "nilai" => $data_nilai

            ];

            $data_nilai = array();

        }



        //Ambil Nilai Per Kriteria


            foreach ($kriteria as $k) {




                $alternatif = Alternatif::join('tbl_kriteria', 'tbl_kriteria.id', '=', 'tbl_alternatif.id_kriteria')
                    ->join('tbl_peserta', 'tbl_peserta.id', '=', 'tbl_alternatif.id_peserta')
                    ->where('tbl_alternatif.id_kriteria', '=', $k->id)
                    ->get();



                if(!$alternatif->isEmpty())
                {
                    $temp = array();
                    foreach ($alternatif as $a){

                        $temp[] = $a->nilai;

                    }
                    $dataKriteria[] = $temp;
                }





            }








        if(!empty($data_alternatif)){

            $daftar_nilai = $data_alternatif[0]["nilai"];

        }



        //Normalisai Data Kriteria
        $dataKriteriaNormalisasi  = array();


        //Prepare max dan min
        //Index untuk ambil nilai bobot
        $index_bobot =0;

        //Ambil Total Bobot
        $totalBobot = 0;

        foreach ($dataKriteria as $data){

            $max = max($data);
            $min = min($data);


            $tempAngkaNormal = array();
            foreach ($data as $d){


                $angkaNormal = @(($max-$d)/($max-$min));

                if(is_nan($angkaNormal)){
                    $angkaNormal = 0;
                }


                $tempAngkaNormal[] = $angkaNormal;




            }
            $dataKriteriaNormalisasi[] = $tempAngkaNormal;

            $totalBobot+= $kriteria[$index_bobot]->bobot;

            $index_bobot++;
        }





        //Ubah Dari Baris Menjadi Kolom untuk di tampilkan
        $dataKriteriaNormalisasiUntukTable = array();

        for($a =0; $a<count($peserta); $a++){


            $temp = array();
            $temp[] = $peserta[$a]->nama_siswa;
            foreach ($dataKriteriaNormalisasi as $d){


                $temp[] = $d[$a];
            }

            $dataKriteriaNormalisasiUntukTable[] = $temp;
        }




        //Data Normalisasi Disesuaikan Dengan Bobot
        //Normalisai Data Kriteria
        $dataKriteriaNormalisasiBobot  = array();


        //Prepare max dan min
        //Index untuk ambil nilai bobot
        $index_bobot =0;

        foreach ($dataKriteria as $data){

            $max = max($data);
            $min = min($data);


            $tempAngkaNormal = array();
            foreach ($data as $d){


                $angkaNormal = @(($max-$d)/($max-$min));

                if(is_nan($angkaNormal)){
                    $angkaNormal = 0;
                }

                $angkaNormal = $angkaNormal*$kriteria[$index_bobot]->bobot*(1/$totalBobot);
                $tempAngkaNormal[] = $angkaNormal;




            }
            $dataKriteriaNormalisasiBobot[] = $tempAngkaNormal;


            $index_bobot++;
        }


        //Ubah Dari Baris Menjadi Kolom untuk di tampilkan
        $dataKriteriaNormalisasiUntukTable2 = array();


        for($a =0; $a<count($peserta); $a++){


            $temp = array();
            $temp[] = $peserta[$a]->nama_siswa;
            foreach ($dataKriteriaNormalisasiBobot as $d){


                $temp[] = $d[$a];
            }

            $dataKriteriaNormalisasiUntukTable2[] = $temp;
        }



        //Menentukan Nilai S
        $daftarS = array();

        $temp = 0;

        for($a =0; $a<count($peserta); $a++){


            foreach ($dataKriteriaNormalisasiBobot as $d){


                $temp += $d[$a];


            }

            $daftarS[] = $temp;
            $temp=0;


        }

        //Menentukan Nilai R

        $daftarR = array();

        $temp = 0;

        for($a =0; $a<count($peserta); $a++){


            $temp = array();
            foreach ($dataKriteriaNormalisasiBobot as $d){


                $temp[] = $d[$a];
            }

            $daftarR[] = max($temp);

        }


        $minDaftarS = min($daftarS);
        $maxDaftarS = max($daftarS);

        $minDaftarR = min($daftarR);
        $maxDaftarR = max($daftarR);

        //

        $daftarQUntukTable = array();

        $daftarQ = array();



        for($x =0 ; $x<count($daftarS) ; $x++){

            $tempData = array();

            $tempData[] = $peserta[$x]->nama_siswa;
            $temp = (($daftarS[$x]-$minDaftarS)/($maxDaftarS-$minDaftarS)*0.5)+(($daftarR[$x]-$minDaftarR)/($maxDaftarR-$minDaftarR)*(1-0.5));


            $tempData[] = $temp;
            $daftarQ[] = [

                "nama" => $peserta[$x]->nama_siswa,
                "nilai" => $temp
            ];

            $daftarQUntukTable[] = $tempData;

        }



        $index = array_search(min($daftarQ), $daftarQ);



        //Peserta Terpilih Adalah
        $p = $daftarQUntukTable[$index][0];


        uasort($daftarQ, function ($a, $b) {
            return ( $a['nilai'] > $b['nilai'] ? 1 : -1 );
        });

        return view('/vikor' , compact("data_alternatif" , "daftar_nilai" , "kriteria" , "dataKriteriaNormalisasiUntukTable" , "dataKriteriaNormalisasiUntukTable2" , "daftarQUntukTable" , "p" , "daftarQ"));

    }


    function mySort($a, $b) {
        if($a['nilai'] == $b['nilai']) {
            return 0;
        }
        return ($a['nilai'] < $b['nilai']) ? 1 : -1;
    }



}


