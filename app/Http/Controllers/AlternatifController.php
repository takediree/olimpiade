<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kriteria;
use App\Models\Peserta;
use App\Models\Alternatif;

class AlternatifController extends Controller
{
    public function index()
    {
        $alternatif = Alternatif::all();         
        $peserta = Peserta::all();       
                        
        return view('alternatif/index', compact('alternatif','peserta'));
    }
    public function create()
    {
        $peserta = Peserta::all();
        
        $kriteria = Kriteria::all();

        return view('alternatif/create',compact('kriteria','peserta'));
    }
    
    public function store(Request $request)
    {
        $request->validate([
            'kriteria' => 'required',
            'bobot' => 'required|numeric',
            
        ]);
        $alternatif = new Alternatif;      
        $alternatif->id_kriteria = $request->id_kriteria;    
        $alternatif->id_peserta = $request->id_peserta;    
        $alternatif->nilai = $request->nilai;    
        
       $alternatif->save();

        return redirect('alternatif')->with('status', 'Data berhasil ditambahkan!');

    }

    public function destroy($id)
    {
        $a = Alternatif::find($id);
        $a->delete();
        return redirect('alternatif')->with('status', 'Data berhasil update!');
    }

    public function edit(Alternatif $alternatif)
    {
        $peserta = Peserta::all();
        $kriteria = Kriteria::all();
        return view('alternatif.update',compact('alternatif','peserta','kriteria'));
    }

    public function update(Alternatif $alternatif,Request $request)
    {
        $alternatif = Alternatif::find($alternatif->id);
        $alternatif->id_kriteria = $request->id_kriteria;    
        $alternatif->id_peserta = $request->id_peserta;    
        $alternatif->nilai = $request->nilai;    
        
       $alternatif->save();
       return redirect('alternatif')->with('status', 'Data berhasil ditambahkan!');
    }

    public function tambahnilai($alternatif)
    {



        $id_peserta = $alternatif;


        $peserta = Peserta::all();


        //buat kustom model data
        $data = array();


        //Pertama Check kalo nilai yang di maksud sudah ada by kriteria dan peserta
        $kriteria = Kriteria::all();

        foreach($kriteria as $k){
            
            $alternatif = Alternatif::where('id_kriteria' , '=' , $k->id)
                                    ->where('tbl_alternatif.id_peserta' , '=' , $id_peserta)
                                    ->first();

            if($alternatif){

                $data[] = array(
                    "id" => $k->id,
                    "nama" => $k->kriteria,
                    "nilai" => $alternatif->nilai
                );

            }else{

                
                $data[] = array(
                    "id" => $k->id,
                    "nama" => $k->kriteria,
                    "nilai" => 0
                );

            }
            
        }
        return view('alternatif.tambahnilai',compact('kriteria','peserta',"id_peserta" , "alternatif", "data"));

    }
    
    
    public function savenilai(Request $request)
    {

      foreach($request->id_kriteria as $key => $value){

            $alternatif = Alternatif::where("id_kriteria" , '=' , $value)
                                    ->where('id_peserta' , '=' , $request->id_peserta)
                                    ->first();
            if($alternatif){
                $alternatif->nilai = $request->nilai[$key];

                $alternatif->save();

            }else{
                $request->validate([
                    
                    'nilai' => 'min:2|max:5',
                    
                ]);

                $alternatif = new Alternatif();
                $alternatif->id_peserta = $request->id_peserta;
                $alternatif->id_kriteria = $value;
                $alternatif->nilai = $request->nilai[$key];

                $alternatif->save();
                
                
            }                        
            
        }
        return redirect('alternatif')->with('status', 'Data berhasil Update!');
}
}

