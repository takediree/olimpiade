<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Peserta;

class PesertaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $peserta= Peserta::all();
        return view('peserta/index',compact('peserta'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $peserta= Peserta::all();
        return view('/peserta/create',compact('peserta'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nis' => 'required|numeric|unique:tbl_peserta,nis',
            
            
        ]);
        $peserta = New Peserta;
        $peserta->nis = $request->nis;
        $peserta->nama_siswa = $request->nama_siswa;
        $peserta->alamat_siswa = $request->alamat_siswa;
       
        
        $peserta->save();
        return redirect('peserta')->with('status', 'Data berhasil ditambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Peserta $peserta)
    {
        return view('peserta/update',compact('peserta'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Peserta $peserta)
    {
        $request->validate([
            'nis' => 'required|numeric|unique:tbl_peserta,nis',
            'bobot' => 'required|numeric',
            
        ]);
        $peserta = Peserta::find($peserta->id);
        
        $peserta->nis = $request->nis;
        $peserta->nama_siswa = $request->nama_siswa;
        $peserta->alamat_siswa = $request->alamat_siswa;
      
        
        $peserta->save();

        return redirect('peserta')->with('status', 'Data berhasil update!');   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $k = Peserta::find($id);
        $k->delete();
        return redirect('peserta')->with('status', 'Data berhasil update!');
    }
}
