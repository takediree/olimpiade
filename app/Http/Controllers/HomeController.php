<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Peserta;
use App\Models\Kriteria;

class HomeController extends Controller
{

   public function home()
        {
            $jumlahpeserta = Peserta::count();
            $jumlahkriteria = Kriteria::count();
            return view('/home', compact('jumlahpeserta','jumlahkriteria'));

        }


}
